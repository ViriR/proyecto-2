PROYECTO 2; 01/10/2019


El proyecto #2 de la materia de Dise�o y An�lisis de Algoritmos consisti� en la construcci�n 
aleatoria de grafos bajo cuatro diferentes m�todos (Erd�s y R�nyi, Gilbert, Geogr�fico simple, 
y Barab�si-Albert); mismo que fue desarrollado en Java (lo que se present� en el "Proyecto 1"). 
Dichos grafos ahora son recorridos mediante dos m�todos: B�squeda en anchura o BFS (Breadth First
Search), y B�squeda en profundidad o DFS (Depth First Search), �sta �ltimo utilizando una t�cnica
iterativa y recursiva.


A continuaci�n se describir� de manera concisa el procedimiento a realizar para la generaci�n
de cualquier tipo de grafo:

1.- Abrir en cualquier editor de Java (el proyecto se realiz� en NetBeans IDE) el proyecto 
titulado "Proyecto2".
2.- Dentro del proyecto podr�n ser visualizadas tres clases: Grafos, Aristas, Nodos. Abrir la
clase "Grafos".
3.- En la clase "Grafos" se encuentra desarrollado el algoritmo pertinente para la generaci�n de
cualquier tipo de grafo (Erd�s y R�nyi, Gilbert, Geogr�fico simple, y Barab�si-Albert). En la parte
final se puede observar el m�todo "main", donde se da la instrucci�n de construir el grafo deseado
y colocarle un nombre al archivo .gv.
4.- Una vez generado el grafo dentro de un arvhivo .gv, podr� ser observado de manera gr�fica a 
trav�s del software Gephi, en donde se abre el archivo correspondiente y el grafo puede ser 
visualizado.
5.- Al construir cualquier tipo de grafo aleatorio, en autom�tico dicho grafo ser� recorrido por los
m�todos de b�squeda mencionados anteriormente (BFS, DFS-I, y DFS-R). Por lo que, se construir�n tres
grafos m�s (dentro de tres archivos .gv diferentes).
6.- Al final se obtendr�n cuatro archivos .gv que podr�n ser visualizados en el software Gephi como
en el proyecto anterior.

- Viridiana Rodr�guez Gonz�lez 