package grafo;

import java.io.FileWriter;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.Stack;

public class Grafos {

    private HashMap<Integer, Nodos> Nodos;
    private HashMap<Integer, Aristas> Aristas;

    public Grafos(HashMap<Integer, Nodos> N, HashMap<Integer, Aristas> A) {
        this.Aristas = new HashMap();
        for (int i = 0; i < A.size(); i++) {
            this.Aristas.put(i, new Aristas(A.get(i)));
        }
        this.Nodos = new HashMap();
        for (int i = 0; i < N.size(); i++) {
            this.Nodos.put(i, new Nodos(N.get(i)));
        }
    }

    public Grafos() {
        this.Aristas = new HashMap();
        this.Nodos = new HashMap();
    }

    public Grafos(Grafos k) {
        this.Aristas = new HashMap();
        for (int i = 0; i < k.getAristas().size(); i++) {
            this.Aristas.put(i, new Aristas(k.getAristas().get(i)));
        }
        this.Nodos = new HashMap();
        for (int i = 0; i < k.getNodos().size(); i++) {
            this.Nodos.put(i, new Nodos(k.getNodos().get(i)));
        }
    }

    public void setNodos(HashMap<Integer, Nodos> w) {
        this.Nodos = w;
    }

    public void setAristas(HashMap<Integer, Aristas> w) {
        this.Aristas = w;
    }

    public HashMap<Integer, Aristas> getAristas() {
        return this.Aristas;
    }

    public HashMap<Integer, Nodos> getNodos() {
        return this.Nodos;
    }

    public void setG(HashMap<Integer, Nodos> a, HashMap<Integer, Aristas> b) {
        this.Nodos = (HashMap) a;
        this.Aristas = (HashMap) b;
    }
    
    public static Grafos ErdosRenyi(int NumNodos, int NumAristas, int dirigido) {
        HashMap<Integer, Nodos> NodoS = new HashMap();
        HashMap<Integer, Aristas> AristaS = new HashMap();

        int AristasHechas;

        for (int i = 0; i < NumNodos; i++) {
            NodoS.put(i, new Nodos(i));
        }

        int x1 = (int) (Math.random() * NumNodos), x2 = (int) (Math.random() * NumNodos);
        AristaS.put(0, new Aristas(NodoS.get(x1).get(), NodoS.get(x2).get(), Math.random()));

        while (x1 == x2 && dirigido == 0) {
            x1 = (int) (Math.random() * NumNodos);
            x2 = (int) (Math.random() * NumNodos);
            AristaS.put(0, new Aristas(NodoS.get(x1).get(), NodoS.get(x2).get(), Math.random()));
        }

        NodoS.get(x1).conectar();
        NodoS.get(x2).conectar();
        if (x1 != x2) {
            NodoS.get(x1).IncGrado(1);
        }
        NodoS.get(x2).IncGrado(1);

        AristasHechas = 1;
        while (AristasHechas < NumAristas) {
            x1 = (int) (Math.random() * NumNodos);
            x2 = (int) (Math.random() * NumNodos);

            if (x1 != x2 || dirigido == 1) {
                int c1 = 1, cont = 0;
                while (c1 == 1 && cont < AristasHechas) {
                    int a = AristaS.get(cont).getAn1(), b = AristaS.get(cont).getAn2();
                    if ((x1 == a && x2 == b) || (x1 == b && x2 == a)) {
                        c1 = 0;
                    }
                    cont++;
                }
                if (c1 == 1) {
                    AristaS.put(AristasHechas, new Aristas(NodoS.get(x1).get(), NodoS.get(x2).get(), Math.random()));
                    NodoS.get(x1).conectar();
                    NodoS.get(x2).conectar();
                    if (x1 != x2) {
                        NodoS.get(x1).IncGrado(1);
                    }
                    NodoS.get(x2).IncGrado(1);
                    AristasHechas++;
                }
            }
        }

       
        Grafos G = new Grafos(NodoS, AristaS);
        return G;

    }
    
    public static Grafos Geografico(int NumNodos, double distancia, int dirigido) {
        HashMap<Integer, Nodos> NodoS = new HashMap();
        HashMap<Integer, Aristas> AristaS = new HashMap();
        int NumAristas = 0;

        for (int i = 0; i < NumNodos; i++) {
            NodoS.put(i, new Nodos(i, Math.random(), Math.random()));
        }

        for (int i = 0; i < NumNodos; i++) {
            for (int j = i; j < NumNodos; j++) {
                if (j != i || dirigido == 1) {
                    double dis = Math.sqrt(Math.pow(NodoS.get(j).getX() - NodoS.get(i).getX(), 2)
                            + Math.pow(NodoS.get(j).getY() - NodoS.get(i).getY(), 2));
                    if (dis <= distancia) {
                        AristaS.put(NumAristas, new Aristas(NodoS.get(i).get(), NodoS.get(j).get()));
                        NodoS.get(i).IncGrado(1);
                        NodoS.get(i).conectar();
                        if (j != i) {
                            NodoS.get(j).IncGrado(1);
                            NodoS.get(j).conectar();
                        }
                        NumAristas++;
                    }
                }
            }
        }
        Grafos G = new Grafos(NodoS, AristaS);
        return G;
    }

    public static Grafos Gilbert(int NumNodos, double probabilidad, int dirigido) {
        HashMap<Integer, Nodos> NodoS = new HashMap();
        HashMap<Integer, Aristas> AristaS = new HashMap();

        int NumAristas = 0;

        for (int i = 0; i < NumNodos; i++) {
            NodoS.put(i, new Nodos(i));
        }

        for (int i = 0; i < NumNodos; i++) {
            for (int j = i; j < NumNodos; j++) {
                if (j != i || dirigido == 1) {
                    if (Math.random() <= probabilidad) {
                        AristaS.put(NumAristas, new Aristas(NodoS.get(i).get(), NodoS.get(j).get()));
                        NodoS.get(i).conectar();
                        NodoS.get(j).conectar();
                        if (i != j) {
                            NodoS.get(i).IncGrado(1);
                        }
                        NodoS.get(j).IncGrado(1);
                        NumAristas++;
                    }
                }
            }
        }
        Grafos G = new Grafos(NodoS, AristaS);
        return G;
    }

    public static Grafos Barabasi(int NumNodos, double d, int dirigido) {
        HashMap<Integer, Nodos> NodoS = new HashMap();
        HashMap<Integer, Aristas> AristaS = new HashMap();

        int NumAristas = 0;

        for (int i = 0; i < NumNodos; i++) {
            NodoS.put(i, new Nodos(i));
        }

        for (int i = 0; i < NumNodos; i++) {
            int j = 0;
            while (j <= i && NodoS.get(i).getGrado() <= d) {
                if (j != i || dirigido == 1) {
                    if (Math.random() <= 1 - NodoS.get(j).getGrado() / d) {
                        AristaS.put(NumAristas, new Aristas(NodoS.get(i).get(), NodoS.get(j).get()));
                        NodoS.get(i).IncGrado(1);
                        NodoS.get(i).conectar();
                        if (j != i) {
                            NodoS.get(j).IncGrado(1);
                            NodoS.get(j).conectar();
                        }
                        NumAristas++;
                    }
                }
                j++;
            }
        }
        Grafos G = new Grafos(NodoS, AristaS);
        return G;
    }

    public static void construir(String nombre, Grafos g) {
        FileWriter fichero = null;
        PrintWriter pw = null;

        System.out.println(g.getNodos().size());

        try {
            fichero = new FileWriter(nombre + ".gv");
            pw = new PrintWriter(fichero);
            pw.println("graph 666{"); //Por Gephi
            for (int i = 0; i < g.getNodos().size(); i++) {
                pw.println(g.getNodos().get(i).get() + "  " + "[Label = \"" + g.getNodos().get(i).get() + " (" + String.format("%.2f", g.getNodos().get(i).getwin()) + ")\"]");
            }
            pw.println();
            for (int i = 0; i < g.getAristas().size(); i++) {
                pw.println(g.getAristas().get(i).getAn1() + "--" + g.getAristas().get(i).getAn2() + "  " + "[Label = \"" + String.format("%.2f", g.getAristas().get(i).getP()) + "\"]");
            }
            pw.println("}");

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (null != fichero) {
                    fichero.close();
                }
            } catch (Exception e2) {
                e2.printStackTrace();
            }
        }
    }
    
    public static Grafos BFS(Grafos GN, Nodos node) {
        Grafos Gr = new Grafos(GN.getNodos(), GN.getAristas());
        HashMap<Integer, HashMap> Col = new HashMap(); 
        HashMap<Integer, Nodos> Col_1 = new HashMap();   // Nodos
        HashMap<Integer, Nodos> Col_2 = new HashMap();   
        HashMap<Integer, Nodos> Col_3 = new HashMap();       
        HashMap<Integer, Aristas> Col_A = new HashMap(); // Aristas
        int nCol = 0, nC = 0,q = 0;
        Gr.getNodos().get(node.get()).setfal(true);
        Col_1.put(0, Gr.getNodos().get(node.get()));
        Col.put(nCol, (HashMap) Col_1.clone());
        Col_3.put(q, Gr.getNodos().get(node.get()));
        while (Col_1.isEmpty() == false) {
            Col_2.clear();
            nC = 0;
            for (int i = 0; i < Col_1.size(); i++) {
                for (int j = 0; j < Gr.getAristas().size(); j++) {
                    if (Col_1.get(i).get() == Gr.getAristas().get(j).getAn1() && Gr.getNodos().get(Gr.getAristas().get(j).getAn2()).getfal() == false) {
                        Gr.getNodos().get(Gr.getAristas().get(j).getAn2()).setfal(true);
                        Col_2.put(nC, Gr.getNodos().get(Gr.getAristas().get(j).getAn2()));
                        nC++;
                        Col_A.put(q, Gr.getAristas().get(j));
                        q++;
                        Col_3.put(q, Gr.getNodos().get(Gr.getAristas().get(j).getAn2()));
                    }
                    if (Col_1.get(i).get() == Gr.getAristas().get(j).getAn2() && Gr.getNodos().get(Gr.getAristas().get(j).getAn1()).getfal() == false) {
                        Gr.getNodos().get(Gr.getAristas().get(j).getAn1()).setfal(true);
                        Col_2.put(nC, Gr.getNodos().get(Gr.getAristas().get(j).getAn1()));
                        nC++;
                        Col_A.put(q, Gr.getAristas().get(j));
                        q++;
                        Col_3.put(q, Gr.getNodos().get(Gr.getAristas().get(j).getAn1()));
                    }
                }
            }
            nCol++;
            Col_1 = (HashMap) Col_2.clone();
            Col.put(nCol, (HashMap) Col_2.clone());
        }
        Grafos ArBFS = new Grafos();
        ArBFS.setG(Col_3, Col_A);
        return ArBFS;
    }
    
    public static Grafos DFS_R(Grafos GN, Nodos node) {
        Grafos ArDFS_R = new Grafos();
        Grafos ArDFSR_2;
        boolean Ar[][] = new boolean[GN.getNodos().size()][GN.getNodos().size()];
        for (int i = 0; i < GN.getAristas().size(); i++) {
            Ar[GN.getAristas().get(i).getAn1()][GN.getAristas().get(i).getAn2()] = true;
            Ar[GN.getAristas().get(i).getAn2()][GN.getAristas().get(i).getAn1()] = true;
        }
        GN.getNodos().get(node.get()).setfal(true);
        ArDFS_R.getNodos().put(0, new Nodos(GN.getNodos().get(node.get())));
        for (int i = 0; i < GN.getNodos().size(); i++) {
            if (Ar[node.get()][i] == true && GN.getNodos().get(i).getfal() == false) {
                ArDFSR_2 = DFS_R(GN, GN.getNodos().get(i));
                int n4 = ArDFS_R.getNodos().size();
                for (int j = 0; j < ArDFSR_2.getNodos().size(); j++) {
                    ArDFS_R.getNodos().put(n4 + j, ArDFSR_2.getNodos().get(j));
                }
                ArDFS_R.getAristas().put(ArDFS_R.getAristas().size(), new Aristas(node.get(), i));
                n4 = ArDFS_R.getAristas().size();
                if (ArDFSR_2.getAristas().isEmpty() != true) {
                    for (int j = 0; j < ArDFSR_2.getAristas().size(); j++) {
                        ArDFS_R.getAristas().put(n4 + j, ArDFSR_2.getAristas().get(j));
                    }
                }
            }
        }
        return ArDFS_R;
    }
    
    public static Grafos DFS_I(Grafos GN, Nodos node) {
        Grafos Gr2 = new Grafos(GN.getNodos(), GN.getAristas());
        Grafos ArDFS_I = new Grafos();
        int p=0, n2 = 0;
        boolean n3;
        boolean Ar[][] = new boolean[Gr2.getNodos().size()][Gr2.getNodos().size()];
        for (int i = 0; i < Gr2.getAristas().size(); i++) {
            if (Gr2.getAristas().get(i).getfal() == false) {
                Ar[Gr2.getAristas().get(i).getAn1()][Gr2.getAristas().get(i).getAn2()]=true;
                Ar[Gr2.getAristas().get(i).getAn2()][Gr2.getAristas().get(i).getAn1()]=true;
            }
        }
        Stack<Integer> heap = new Stack<>();
        heap.push(Gr2.getNodos().get(node.get()).get());
        Gr2.getNodos().get(node.get()).setfal(true);
        ArDFS_I.getNodos().put(n2, new Nodos(Gr2.getNodos().get(node.get())));
        while (heap.isEmpty() == false) {
            p = heap.peek();
            n3 = false;
            for (int j = 0; j < Gr2.getNodos().size(); j++) {
                if (Ar[p][j] == true && Gr2.getNodos().get(j).getfal() == false) {
                    Gr2.getNodos().get(j).setfal(true);
                    ArDFS_I.getAristas().put(n2, new Aristas(p, j));
                    n2++;
                    ArDFS_I.getNodos().put(n2, new Nodos(Gr2.getNodos().get(j)));
                    heap.push(j);
                    n3 = true;
                    j = Gr2.getNodos().size();
                }
                if (j == Gr2.getNodos().size() - 1 && n3 == false) {
                    heap.pop();
                }
            }
        }
        return ArDFS_I;
    }
    
    public static void main(String[] args) {

        Grafos GN = new Grafos();
        //GN = ErdosRenyi(500,1500,0);
        //GN= Geografico(500,.5,0);
        //GN = Gilbert(500,.3,0);
        GN = Barabasi(500,700,0); 
        Grafos GBFS = new Grafos(BFS(GN,GN.getNodos().get(0)));
        Grafos GDFSI = new Grafos(DFS_I(GN,GN.getNodos().get(0)));
        Grafos GDFSR = new Grafos(DFS_R(GN,GN.getNodos().get(0)));
        // Construir todos los grafos y sus árboles:
        construir("grafoB3_original",GN);
        construir("grafoB3_BFS",GBFS);
        construir("grafoB3_DFS-I",GDFSI);
        construir("grafoB3_DFS-R",GDFSR);
    }
}
